const book = require("./models/book").book;

async function getAllBooks() {
  let data = await book.find().lean();
  return { data };
}

async function createBook(bookData) {
  return new Promise((resolve, reject) => {
    let newBook = new book(bookData);
    newBook.save(function (err, result) {
      if (err) {
        reject();
      } else {
        resolve(result);
      }
    });
  });
}

async function updateBook(id, bookData) {
  return new Promise(async function (resolve, reject) {
    book.findOneAndUpdate(
      { _id: id },
      { $set: bookData },
      { new: false },
      (err, updated) => {
        if (!updated || err) {
          reject();
        } else {
          resolve(updated);
        }
      }
    );
  });
}

async function deleteBook(id) {
  return new Promise(async function (resolve, reject) {
    book.deleteOne({ _id: id }, function (err, result) {
      console.log(result);
      if (err || !result || result.deletedCount === 0) {
        reject();
      } else {
        resolve({ message: "Book deleted", data: {} });
      }
    });
  });
}

module.exports = {
  getAllBooks,
  createBook,
  updateBook,
  deleteBook,
};
