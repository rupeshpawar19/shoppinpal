
let mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var bookSchema = new Schema(
  {
    author: { type: String, required: true },
    title: { type: String, required: true },
    ISBN: { type: String, required: true },
    releaseDate: { type: String, required: false }
  },
  { timestamps: false }
);


let book = mongoose.model('books', bookSchema);

module.exports = {
  book: book,
};
