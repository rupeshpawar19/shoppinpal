let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require("../index");
let should = chai.should();

chai.use(chaiHttp);

describe("Books", () => {
  // get the books

  describe("/GET books", () => {
    it("it should GET all the books", (done) => {
      chai
        .request(server)
        .get("/books")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.data.should.be.a("array");
          done();
        });
    });
  });

  // Create the book

  describe("/POST book", () => {
    it("it should not POST a book", (done) => {
      let book = {
        author: "Douglas Crockford",
        title: "JavaScript: The Good Parts",
        ISBN: "234",
      };
      chai
        .request(server)
        .post("/books")
        .send(book)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.data.should.have.property("_id");
          done();
        });
    });
  });

  // get the all books and length should be more than 1.

  describe("/GET books", () => {
    it("it should have at least one book after creation", (done) => {
      chai
        .request(server)
        .get("/books")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.at.least(1);
          done();
        });
    });
  });

});
