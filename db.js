module.exports = function (mongoose) {

    var connect = function () {
        mongoose.connect(process.env.DB_URL,  { useNewUrlParser: true });
    };
    connect();

    // Error handler
    mongoose.connection.on('error', function (err) {
        console.error('MongoDB Connection Error. Please make sure MongoDB is running. -> ' + err);
    });

    // Reconnect when closed
    mongoose.connection.on('disconnected', function () {
        connect();
    });
    

    mongoose.connection.once('open', function() {
        // we're connected!
        console.log("Connected")
    });

};