require('dotenv').config();
let http = require("http");
let {
  getAllBooks,
  createBook,
  updateBook,
  deleteBook,
} = require("./controllers");
let mongoose = require("mongoose");
require("./db")(mongoose);

let server = http
  .createServer(async function (req, res) {
    res.writeHead(200, { "Content-Type": "application/json" });
    const headers = {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'OPTIONS, POST, GET',
      'Access-Control-Max-Age': 2592000
    };
  
    if (req.method === 'OPTIONS') {
      res.writeHead(204, headers);
      res.end();
      return;
    }
  
    let url = req.url;
    let method = req.method;

    if (url === "/books" && method === "GET") {
      // GET all Books
      let data = await getAllBooks();
      res.end(JSON.stringify(data));
    } else if (url === "/books" && method === "POST") {
      // Create book
      let data = "";
      req.on("data", (chunk) => {
        data += chunk;
      });
      req.on("end", async function () {
        try {
          data = JSON.parse(data);
          if(!data.author){
            res.end(JSON.stringify({ message: "Please provide author" }));
          }
          if(!data.title){
            res.end(JSON.stringify({ message: "Please provide title" }));
          }
          if(!data.ISBN){
            res.end(JSON.stringify({ message: "Please provide ISBN" }));
          }
          let bookData = await createBook(data);
          res.end(JSON.stringify({ data: bookData, message: "Book created" }));
        } catch (error) {console.log(error)
          res.end(JSON.stringify({ message: "Book creation failed" }));
        }
      });
    } else if (url.startsWith("/books/") && method === "PUT") {
      // Update book
      let data = "";
      req.on("data", (chunk) => {
        data += chunk;
      });
      req.on("end", async function () {
        try {
          let id = url.replace("/books/", "");
          data = JSON.parse(data);
          if(!data.author){
            res.end(JSON.stringify({ message: "Please provide author" }));
          }
          if(!data.title){
            res.end(JSON.stringify({ message: "Please provide title" }));
          }
          if(!data.ISBN){
            res.end(JSON.stringify({ message: "Please provide ISBN" }));
          }
          if(!id){
            res.end(JSON.stringify({ message: "Please provide book ID" }));
          }
          let bookData = await updateBook(id, data);
          res.end(JSON.stringify({ data: bookData, message: "Book updated" }));
        } catch (error) {
          res.end(JSON.stringify({ message: "Book update failed" }));
        }
      });
    } else if (url.startsWith("/books/") && method === "DELETE") {
      // Delete book
      try {
        let id = url.replace("/books/", "");
        if(!id){
          res.end(JSON.stringify({ message: "Please provide book ID" }));
        }
        let data = await deleteBook(id);
        res.end(JSON.stringify(data));
      } catch (error) {
        res.end(JSON.stringify({ message: "Book delete failed" }));
      }
    } else {
      res.end(JSON.stringify({ message: "URL not found" }));
    }
  })
  .listen(3000, function () {
    console.log("server start at port 3000");
  });

  module.exports = server
