# README #

This is Assignment done for Shoppinpal.  

### Run the server ###

* Please make sure you have .env file with DB_URL with MongoDb. eg mongodb://localhost/shoppinpal
* npm install. Please use nodejs version 12.
* npm run start

### Unit tests ###

* npm run test. Please make sure you have dev dependencies installed.